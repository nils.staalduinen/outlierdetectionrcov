## Getting started

Using a virtual environment is highly recommended because it allows you to create isolated environments with their own dependencies, without interfering with other Python projects or the system Python installation. This ensures that your Python environment remains consistent and reproducible across different machines and over time. To create one, type in the following command in your terminal:

```bash
 python3 -m venv path/to/venv
```
To activate the enviroment, type in (not needed for windows):
```bash
 source path/to/venv/bin/activate
```
After activation, you need two packages BEFORE installation, as it is needed for the installation process itself (compiling Fortran). The other dependencies are installed automatically.

```bash
pip install -r requirements.txt
```

Now, you can start with the analysis.ipynb file. 